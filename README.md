# Code challenge


## Installation

Node version used/required - in `.nvmrc` file.

```
npm ci
```


## Task 1 - Problem solving

Make the test in `./tests` folder pass. More info about the individual
challenges in the files.

```
npm run first
```


## Task 2 - Component challenge

Read more instruction in `./src/AutoComplete/AutoComplete.js` file.

```
npm run second
```
