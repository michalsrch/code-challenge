/**
 * Implement function that will test (returns boolean) if given word is an
 * palindrome - reads the same backwards as forwards.
 */

export default function isPalindrome(word) {
  return word
}
