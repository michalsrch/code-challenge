/**
 * We have a data with results of some competition and we want to get results.
 * Each participant belongs to a `group` and has list of `points`. Only the
 * participants that have more than 250 points can qualify and only one of each
 * group with the most points is the winner. If there is a group where no one
 * has more than the required points the group is excluded from the results.
 *
 * Complete the function which takes a data (see structure in ./data.js) and
 * returns an object with the key being the group name and the value being the
 * item id from the data.
 */

export default function dataManipulation(data) {
  return data
}
